import os
import sys

if True:
    srctex = '../velvib.tex'
    srcbbl = '../velvib.bbl'
    arxtemp = 'arx-temp.tex'
    arxdest = 'manuscript.tex'
    figprefix = ''
    
os.system('cp %s %s' % (srctex, arxtemp))

fbbl = open(srcbbl, 'r')
bbl_content = fbbl.read()
fbbl.close()

farx = open(arxdest, 'w')
ftmp = open(arxtemp, 'r')
farx.write('% _,,_\n')
fig_index = 0
subfig_index = 0
orig_figure_files = []
new_figure_files = []
for line in ftmp:
    lproc = line.strip()
    if lproc.startswith('%'):
        print 'Filtering out comment: ',lproc
    elif lproc.startswith('\\bibliography{'):
        print 'Filtering out BibTeX line.'
    elif lproc.startswith('\\bibliographystyle{'):
        print 'Inserting .bbl file content.'
        farx.write(bbl_content)
    elif lproc.startswith('\\includegraphics'):
        # extract name of figure file
        tokA = lproc.split('{')
        tokB = tokA[1].split('}')
        oldfigure_file = tokB[0].strip()
        newfigure_file = '%sfigure%02d%c' % (figprefix,fig_index,'abcdefghijklmnopqrstuvwxyz'[subfig_index])
        # print to .tex file
        print 'fig_index = ',fig_index,'%',subfig_index,'figure_file = ',oldfigure_file
        figline = line.replace(oldfigure_file, newfigure_file)
        lout = figline.replace('\r\n',os.linesep)
        farx.write(lout)
        # update figure file list
        orig_figure_files.append(oldfigure_file)
        new_figure_files.append(newfigure_file)
        subfig_index += 1
    else:
        if lproc.startswith('\\begin{figure'):
            fig_index += 1
            subfig_index = 0
        lout = line.replace('\r\n',os.linesep)
        farx.write(lout)
ftmp.close()
farx.close()

# delete temporary file
os.system('rm %s' % arxtemp)

# copy figure files
for k in range(len(orig_figure_files)):
    origff = '../' + orig_figure_files[k]
    newff = new_figure_files[k]

    FigExtL = ['.pdf','.eps','.png']
    expl_fig_ext = ''
    for fe in FigExtL:
        if origff.endswith(fe):
            expl_fig_ext = fe
    if len(expl_fig_ext) > 0:
        newff = newff + expl_fig_ext
        print 'Copying from %s to %s' % (origff, newff)
        os.system('cp %s %s' % (origff, newff))
    else:
        for fe in FigExtL:
            if os.path.exists(origff + fe):
                print 'Copying from %s to %s' % (origff+fe, newff+fe)
                os.system('cp %s %s' % (origff+fe, newff+fe))
